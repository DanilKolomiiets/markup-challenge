import '../static/wish-list.png'
import "../src/styles/index.css";

function removeItem() {
    const allItems = document.querySelectorAll('.delete-button');
    const removeAllButton = document.querySelector('.remove-all-button')
    const fullItemsList = document.querySelector('#carouselIndicators')
    for (let i = 0; i < allItems.length; i++) {
        allItems[i].addEventListener("click", function() {
            this.closest('.card').style.display = 'none';
        });
    }
    removeAllButton.addEventListener("click", function() {
        fullItemsList.innerHTML = 'Nothing here...'
    })
}

// function findItems() {
//     const findButton = document.querySelector('#find-button');
//     const findArea = document.querySelector('.find-area');
//     const allItems = document.querySelectorAll('.card-title');
//     findButton.addEventListener("click", function() {
//         allItems.forEach(i => {
//             if (findArea.value.toUpperCase() !== i.textContent.toUpperCase()) {
//                 console.log(`Not our request ${i.textContent}`)
//             } else {
//                 console.log(`Thats fine ${i.textContent}`)
//             }
//         })
//     })

// }

// findItems()

window.onload = () => {
    removeItem()
    findItems()
}

function findItems() {
    let list = document.querySelectorAll('.card-title');
    let input = document.querySelector('#find-input')
    if (input) {
        input.oninput = function() {
            let value = this.value.trim().toUpperCase()
            if (value) {
                list.forEach(elem => {
                    if (elem.innerText.toUpperCase().search(value) == -1) {
                        elem.closest('.card').classList.add("hide") //Hide here
                    }
                })
            } else {
                list.forEach(elem => {
                    elem.closest('.card').classList.remove("hide")
                })
            }
        }
    }

}